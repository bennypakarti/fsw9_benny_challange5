const express = require('express');
const path = require("path");
const router = express.Router();
const res = require('express/lib/response');
const app = express()
const { Cars } = require('./models')
const { Ukur } = require('./models')
// const { Campur } = require []

// const { }

const port = process.env.port || 8015;
router.use(express.static(__dirname + "./public/"))

app.use(express.json())

// Path ke directory public
// Yang bakal kita jadikan public
// Sehingga user bisa akses CSS dan Javascript
// Di browser
const PUBLIC_DIRECTORY = path.join(__dirname, "./public");

// Set format request
app.use(express.urlencoded({ extended: true }));

// Set PUBLIC_DIRECTORY sebagai
// static files di express
app.use(express.static(PUBLIC_DIRECTORY));
app.use('/public/upload', express.static(__dirname + '/public/upload'))

// Bilang ke express kalo kita mau
// pake EJS sebagai view engine
app.set("view engine", "ejs");

// GET /car
// app.get("/", (req, res) => {
//     res.render("index");
// });

// coba up foto
global.__basedir = __dirname;
const fs = require('fs');
// const fsPromises = require('fs').promises;
const multer = require("multer");
// const imageFilter = (req, file, cb) => {
//     if (file.mimetype.startsWith("image")) {
//         cb(null, true);
//     } else {
//         cb("Please upload only images.", false);
//     }
// };
const diskStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path.join(__dirname + "/public"));
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + "-" + Date.now() + path.extname(file.originalname));
    },
});
const uploadFile = multer({ storage: diskStorage });
// module.exports = uploadFile;
// end coba

app.get("/addcar", (req, res) => {
    Ukur.findAll(
        {
            order: ['id']
        }
    )
        .then((ukur) => {
            res.render("addcar", { ukur })
            // res.status(200).json(cars)
        })
});

// get all cars
app.get('/', (req, res) => {
    Cars.findAll(
        {
            order: ['id']
        }
    )
        .then((cars) => {
            res.render("index", { cars })
            // res.status(200).json(cars)
        })
})
app.get('/cars', (req, res) => {
    Cars.findAll(
        {
            order: ['id']
        }
    )
        .then((cars) => {
            res.render("index", { cars })
            // res.status(200).json(cars)
        })
})

// GET car by ID
app.get('/cars/:id', (req, res) => {
    Cars.findOne({
        where: { id: req.params.id }
    })
        .then(cars => {
            res.status(200).json(cars)
        })
})

// POST a cars coba
app.post('/cars/add', uploadFile.single("photo"), (req, res) => {
    Cars.create({
        size_id: req.body.size_id,
        name: req.body.name,
        rentPerDay: req.body.rentPerDay,
        photo: req.file.filename
        // photo: fs.readFileSync(
        //     __dirname + "/public/upload/" + req.file.filename
        // ),
    })
        // console.log(cars.name)
        .then((cars) => {
            // fs.writeFileSync(
            //     __dirname + "/public/upload/tmp/" + cars.name, cars.photo
            // );
            // res.redirect(`http://localhost:${port}/cars`)
            // return res.send(`File has been uploaded.`);
            // res.status(201).json(cars)
            res.redirect("/");
        }).catch(err => {
            res.status(422).json("Can't create car")
        })
})

// POST a cars
// app.post('/cars/add', (req, res) => {
//     Cars.create({
//         size_id: req.body.size_id,
//         name: req.body.name,
//         rentPerDay: req.body.rentPerDay,
//         photo: req.body.photo
//     })
//         .then(cars => {
//             // res.status(201).json(cars)
//             // res.redirect(200, "/add");
//         }).catch(err => {
//             res.status(422).json("Can't create car")
//         })
// })

// get form update
app.get('/cars/:id/update', async (req, res) => {
    const cars = await Cars.findOne({
        where: { id: req.params.id },
        include: [{
            model: Ukur
        }]
    })

    const coba = await Ukur.findAll()
    res.render("edit", { cars, coba })

    // ).then(cars => {
    //     // console.log(cars);
    //     res.render("edit", { cars })
    //     // res.send(cars);
    // })
    // Ukur.findAll(
    //     {
    //         order: ['id']
    //     }
    // ).then(ukur => {
    //     res.render("edit", { ukur })
    // })
})

// short small
app.get('/short/:id/cari', (req, res) => {
    Cars.findAll({
        order: ['id'],
        where: { size_id: req.params.id },
    }).then(cars => {
        res.render("index", { cars })
    })
})

// app.get('/short', (req, res) => {
//     Cars.findAll({
//         where: { size_id: req.body.size_id },
//     }).then(cars => {
//         res.render("index", { cars })
//     })
// })

// update a car
app.post('/cars/update/:id', uploadFile.single("photo"), (req, res) => {
    Cars.update({
        size_id: req.body.size_id,
        name: req.body.name,
        rentPerDay: req.body.rentPerDay,
        photo: req.file.filename
    }, {
        where: { id: req.params.id }
    })
        .then(() => {
            res.redirect("/");
        }).catch(err => {
            res.status(422).json("Can't update car")
        })
})


// Delete car by ID
app.get('/cars/delete/:id', (req, res) => {
    Cars.destroy({
        where: { id: req.params.id }
    })
        .then(() => {
            res.redirect("/");
        })
})


app.listen(port, () => console.log(`Listening on http://localhost:${port}`));